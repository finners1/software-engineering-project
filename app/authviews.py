from app import app, db
from flask import render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from app.models import User
from flask_login import login_required, current_user, login_user, logout_user


@app.route('/login', methods=['GET','POST'])
def login():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    if not email or not password:
        flash('Please enter all fields.')
        return render_template("login.html")

    user = User.query.filter_by(email=email).first()


    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect('/login')

    if user or check_password_hash(user.password, password):
        if user.staff == 1:
            login_user(user, remember=remember)
            return redirect('/staffchoosefilm')
        flash("Sucessfully logged in")
        login_user(user, remember=remember)
        return redirect('/home')

    return render_template("login.html")

@app.route('/signup', methods=['GET', 'POST']) 
def signup():
    email = request.form.get('email')
    
    name = request.form.get('name')
    password = request.form.get('password')
    user = User.query.filter_by(email=email).first()
    app.logger.warning(user)
    if not email or not password or not name:
        flash('Please enter all fields')
        return render_template("signup.html")
    if not user:
        flash('Success')
        new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))
        db.session.add(new_user)
        db.session.commit()
        return render_template("login.html")

    if user: 
        flash('Email address already exists')
        return render_template("signup.html")

    

@app.route('/logout')
@login_required
def logout():
    email = request.form.get('email')
    logout_user()
    
    return render_template("login.html")