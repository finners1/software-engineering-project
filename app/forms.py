from flask_wtf import Form
from wtforms import TextField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length
import string

class PaymentForm(Form):
    time = TextField('time', validators=[DataRequired()])
    cardNumber = TextField('cardNumber', validators=[DataRequired(), Length(min=16, max=16)])
    cvv = TextField('cvv', validators=[DataRequired(), Length(min=3, max=3)])
    expiryDate = TextField('expiryDate', validators=[DataRequired(), Length(min=5,max=5)])
    children = SelectField('children', choices=[0,1,2,3,4,5,6,7,8])
    adults = SelectField('adult', choices=[0,1,2,3,4,5,6,7,8])
    seniors = SelectField('senior', choices=[0,1,2,3,4,5,6,7,8])
    date = TextField('date')
    movieID = TextField('movieID')
    totalCost = TextField('totalCost')

class CustomersForm(Form):
    times = SelectField('times', validators=[DataRequired()])
    children = SelectField('children', choices=[0,1,2,3,4,5,6,7,8])
    adults = SelectField('adult', choices=[0,1,2,3,4,5,6,7,8])
    seniors = SelectField('senior', choices=[0,1,2,3,4,5,6,7,8])
    date = TextField('date')
    movieID = TextField('movieID')

class StaffForm(Form):
    movies = SelectField('movies')
    times = SelectField('times')
    currentMovie = TextField('currentMovie')
    paymentMethod = SelectField('paymentMethod', choices=['Cash','Card'])
    children = SelectField('children', choices=[0,1,2,3,4,5,6,7,8])
    adults = SelectField('adult', choices=[0,1,2,3,4,5,6,7,8])
    seniors = SelectField('senior', choices=[0,1,2,3,4,5,6,7,8])
    paymentAmount = IntegerField('paymentAmount')
    totalCost = TextField('totalCost')


