from app import app, db
from app.models import Movie, Screening, Booking
from flask import render_template, flash, request, url_for, redirect
from flask_login import login_required, current_user
from flask import request
from .forms import PaymentForm, CustomersForm, StaffForm
import json
import random
from datetime import date
import collections


@app.route('/')
def index():
    return redirect('/listings')

@app.route('/home')
def home():
    allMovies = Movie.query.all()
    return render_template("listings.html", allMovies=allMovies)

@app.route('/listings')
def list():
    allMovies = Movie.query.all()
    return render_template("listings.html", allMovies=allMovies)

@app.route('/mybookings')
@login_required
def mybookings():

    myBookings = Booking.query.filter_by(u_id=current_user.id).all()
    myScreenings = []
    myMovies = []

    for booking in myBookings:
        screeningID = booking.s_id
        p = Screening.query.filter_by(id = screeningID).first()
        myScreenings.append(p)

    for screening in myScreenings:
        movieID = screening.m_id
        movieDate = screening.date
        movieTime = screening.time
        eachMovie = Movie.query.filter_by(id = movieID).first()
        myMovies.append(eachMovie.movieName +' on ' + movieDate + ' at ' + movieTime)

    uniqueMovies = []
    for i in myMovies:
        if i not in uniqueMovies:
            uniqueMovies.append(i)

    occurrences = collections.Counter(myMovies)

    movieCount = []

    
    for movie in uniqueMovies:
        occ=occurrences[movie]
        movieCount.append('You have ' + str(occ) + ' tickets for : ' + movie)


    return render_template("mybookings.html", movieCount=movieCount)


@app.route("/movie")
def movie():

    movie_id = request.args.get('movie_id')
    currentDate = request.args.get('date')

    currentMovie = Movie.query.get_or_404(movie_id)
    
    allScreenings = Screening.query.filter_by(m_id=movie_id, date=currentDate).all()


    return render_template('moviepage.html', currentMovie=currentMovie, currentID=movie_id, allScreenings=allScreenings)



@app.route('/makebooking', methods=["GET"])
@login_required
def makebooking():

    currentDate = request.args.get('date')
    currentID = request.args.get('movie_id')

    allScreenings = Screening.query.filter_by(date=currentDate, m_id=currentID).all()

    
    return render_template('makebooking.html', allScreenings=allScreenings)


@app.route('/paymentconfirmation', methods=["GET", "POST"])
@login_required
def paymentconfirmation():
    paymentForm = PaymentForm()
    form = CustomersForm()

    currentMovie = Movie.query.get(form.movieID.data)
    time=paymentForm.time.data
    adults=paymentForm.adults.data
    children=paymentForm.children.data
    seniors=paymentForm.seniors.data
    date=paymentForm.date.data

    totalAdults = round((int(adults)*9.95), 2)
    totalKids = round((int(children)*6.95), 2)
    totalSeniors = round((int(seniors)*5.95), 2)

    totalCost = round((totalAdults + totalKids + totalSeniors), 2)

    screening = Screening.query.filter_by(m_id=currentMovie.id, time=time, date=date).first()

    seat_no = random.randint(0,100)

    if paymentForm.validate_on_submit():
        for x in range(int(adults)) :
            b=Booking(s_id = screening.id, u_id = current_user.id, cost = 9.95, seat=seat_no)
            db.session.add(b)
            db.session.commit()
        for x in range(int(children)) :
            b=Booking(s_id = screening.id, u_id = current_user.id, cost = 6.95, seat=seat_no)
            db.session.add(b)
            db.session.commit()
        for x in range(int(seniors)) :
            b=Booking(s_id = screening.id, u_id = current_user.id, cost = 5.95, seat=seat_no)
            db.session.add(b)
            db.session.commit()
        return render_template("paymentconfirmation.html", form=form)
    else:
        flash('Payment info is not correct format - please try again')
        return render_template("makebooking.html", totalCost=totalCost, currentMovie=currentMovie, time=time, date=date, form=form, paymentForm=paymentForm, totalAdults=totalAdults, totalKids=totalKids, totalSeniors=totalSeniors)

@app.route('/choosetime', methods=["GET", "POST"])
@login_required
def choosetime():
    form = CustomersForm()
    paymentForm = PaymentForm()


    movieID =  form.movieID.data
    time = form.times.data
    date = form.date.data

    currentMovie = Movie.query.get(movieID)

    adults=form.adults.data
    children=form.children.data
    seniors=form.seniors.data

    totalAdults = round((int(adults)*9.95), 2)
    totalKids = round((int(children)*6.95), 2)
    totalSeniors = round((int(seniors)*5.95), 2)

    totalCost = round((totalAdults + totalKids + totalSeniors), 2)


    if form.validate_on_submit():
        return render_template('makebooking.html')
    return render_template('makebooking.html', totalCost=totalCost, currentMovie=currentMovie, time=time, date=date, form=form, paymentForm=paymentForm, totalAdults=totalAdults, totalKids=totalKids, totalSeniors=totalSeniors)

@app.route('/staffchoosefilm')
@login_required
def staffchoosefilm():
    form = StaffForm()

    allMovies = Movie.query.all()

    form.movies.choices = [(movie.movieName) for movie in allMovies]

    return render_template('staffchoosefilm.html', currentUser = current_user, allMovies=allMovies, form=form)


@app.route('/staffchoosetime', methods=['POST', 'GET'])
@login_required
def staffchoosetime():
    form = StaffForm()
    currentMovie = form.movies.data
    currentMovie = Movie.query.filter_by(movieName=currentMovie).first()
    today = date.today()
    fDate = today.strftime("%m/%d/%Y")
    app.logger.warning(fDate)
    allScreenings = Screening.query.filter_by(m_id=currentMovie.id, date=fDate).all()
    form.times.choices = [(screening.time) for screening in allScreenings]
    
    
    return render_template('staffchoosetime.html', date=fDate, currentMovie=currentMovie, form=form)

@app.route('/cashpayment', methods=['POST', 'GET'])
@login_required
def cashpayment():
    form=StaffForm()
    paymentForm=PaymentForm()
    
    formCurrentMovie = form.currentMovie.data
    currentMovie = Movie.query.filter_by(id=formCurrentMovie).first()
    time=form.times.data
    today = date.today()
    fDate = today.strftime("%m/%d/%Y")

    adults=form.adults.data
    children=form.children.data
    seniors=form.seniors.data

    totalAdults = round((int(adults)*9.95), 2)
    totalKids = round((int(children)*6.95), 2)
    totalSeniors = round((int(seniors)*5.95), 2)

    totalCost = round((totalAdults + totalKids + totalSeniors), 2)
    
    if form.paymentMethod.data == 'Card':
        return render_template('cardpayment.html', totalCost=totalCost, currentMovie=currentMovie, time=time, date=fDate, form=form, paymentForm=paymentForm, totalAdults=totalAdults, totalKids=totalKids, totalSeniors=totalSeniors)

    return render_template('cashpayment.html', totalCost=totalCost, currentMovie=currentMovie, time=time, date=fDate, form=form, paymentForm=paymentForm, totalAdults=totalAdults, totalKids=totalKids, totalSeniors=totalSeniors)


@app.route('/staffcashpaymentconfirmation', methods=['POST', 'GET'])
@login_required
def staffcashpaymentconfirmation():
    form=StaffForm()
    
    totalCost = form.totalCost.data
    amountPaid = form.paymentAmount.data

    changeDue = round((float(amountPaid) - float(totalCost)), 2)

    return render_template('staffcashpaymentconfirmation.html', changeDue=changeDue)


@app.route('/staffcardpaymentconfirmation', methods=['POST', 'GET'])
@login_required
def staffcardpaymentconfirmation():
    return render_template('staffcardpaymentconfirmation.html')


@app.route('/respond', methods=['POST', 'GET'])
@login_required
def respond():
    form = CustomersForm()
    data = json.loads(request.data)
    date = data.get('date')
    movieID = data.get('movie')

    allMovies = Movie.query.all()

    allScreenings = Screening.query.filter_by(date=date, m_id=movieID).all()

    currentMovie = Movie.query.get(movieID)

    if len(allScreenings) == 0:
        flash('There are no screenings for this date - please pick another date')
        return render_template('listings.html', allMovies = allMovies)


    form.times.choices = [(screening.time) for screening in allScreenings]

    
    return render_template('choosetime.html', allScreenings=allScreenings, currentMovie=currentMovie, date=date, form=form)

    

