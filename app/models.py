from app import db
from flask_login import UserMixin

class User(UserMixin, db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)
    password = db.Column(db.String)
    name = db.Column(db.String)
    staff = db.Column(db.Boolean, default=False)

class Movie(db.Model):
    __tablename__ = "movie"
    id = db.Column(db.Integer, primary_key=True)
    movieName = db.Column(db.String)
    movieDescription = db.Column(db.String)
    ageRating = db.Column(db.String)
    length = db.Column(db.Integer)
    director = db.Column(db.String)
    leadact = db.Column(db.String)
    screening = db.relationship("Screening")

class Screening(db.Model):
    __tablename__ = "screening"
    id = db.Column(db.Integer, primary_key=True)
    m_id = db.Column(db.Integer, db.ForeignKey("movie.id"))
    time = db.Column(db.String)
    date = db.Column(db.String)
    screen = db.Column(db.Integer)
    movie = db.relationship("Movie")
    

class Booking(db.Model):
    __tablename__ = "booking"
    id = db.Column(db.Integer, primary_key=True)
    s_id = db.Column(db.Integer, db.ForeignKey("screening.id"))
    u_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    cost = db.Column(db.String)
    seat = db.Column(db.Integer)
    screening = db.relationship("Screening")
    user = db.relationship("User")



